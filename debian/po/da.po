# Danish translation Dash.
# Copyright (C) 2010 Dash & nedenstående oversættere.
# This file is distributed under the same license as the Dash package.
# Claus Hindsgaul <claus_h@image.dk>, 2004.
# Claus Hindsgaul <claus.hindsgaul@gmail.com>, 2006.
# Joe Hansen <joedalton2@yahoo.dk>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: dash\n"
"Report-Msgid-Bugs-To: dash@packages.debian.org\n"
"POT-Creation-Date: 2009-07-27 12:43+0000\n"
"PO-Revision-Date: 2010-06-03 17:30+01:00\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org> \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../dash.templates.in:1001
msgid "Use dash as the default system shell (/bin/sh)?"
msgstr "Brug dash som standardsystemskal (/bin/sh)?"

#. Type: boolean
#. Description
#: ../dash.templates.in:1001
msgid "The system shell is the default command interpreter for shell scripts."
msgstr "Systemskallen er standardkommandofortolkeren til skalskript."

#. Type: boolean
#. Description
#: ../dash.templates.in:1001
msgid ""
"Using dash as the system shell will improve the system's overall "
"performance. It does not alter the shell presented to interactive users."
msgstr ""
"Brug af dash som systemskal vil forbedre systemets generelle ydeevne. Den "
"ændrer ikke på skallen vist til interaktive brugere."
